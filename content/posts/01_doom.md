+++
title = "An Introduction to Doom Emacs"
date = "2020-11-03T13:53:42-03:00"
author = "mrbig"
authorTwitter = "" #do not include @
cover = ""
tags = ["", ""]
keywords = ["", ""]
description = ""
showFullContent = false
+++

# Introdution

## What is Doom?

Doom is an [Emacs](https://www.gnu.org/software/emacs/download.html) distribution for [Vim](https://www.vim.org/) an [Evil](https://github.com/emacs-evil/evil) users, providing a robust yet performant Vim-like emulation layer with minimal compromises. It achieves this goal with just the right level of abstraction, allowing the informed user to further tailor their Emacs experience with handy macros and a simple file structure that keeps everything neatly organized. It&rsquo;s also extremely fast, with crazy optimization. Even with sixteen extra packages (for a total of 156 across 26 modules) and 600 lines of custom lisp configuration, it takes about 1.4 seconds for Emacs to boot from my SSD and become. That&rsquo;s outstanding!

## Why should I use Doom?

Before Doom, I didn&rsquo;t like the idea of using an Emacs distribution. My configuration had about 4000 lines, and was a complete reflection of my journey to learn Emacs as well as my personal tastes. It was a complete mess, but I loved it because it was mine. But, about six months, I was ready to quit. The amount of breakages my ignorance introduced on my day to day life was becoming unbearable. Besides, installing new packages required hours of experimentation create suitable keybindings that played nice my Evil installation. The [Evil Collection](https://github.com/emacs-evil/evil-collection) is outstanding, providing keybindings to dozens of modes, but it still requires lots of manual intervention. Some packages have no Evil settings at all, which is always a pain.

Much like Spacemacs, Doom Emacs comes with sets of integrated packages (modules) for different activities, such as Python, Org Mode, and Markdown. The macros help with common configuration tasks, like adding hooks, setting keybindings, etc. This creates concise yet readable code that are easy to organize on the three user init files: `init.el` (to activate Doom&rsquo;s modules), `packages.el` (for packages not provided by any modules), `config.el` (for personal settings).

# Installation

The install process is straightforwards, just run these commands in the command line:

    $ git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d
    $ ~/.emacs.d/bin/doom install

After that, running \`emacs\` from the command line or your desktop launcher should greet you with the Doom dashboard, which gives you some useful self-explanatory shortcuts, as well as the total loading time. In this case, Doom Emacs took 0.9 seconds to load. Quite a feat for such a hefty distribution!
![img](media/01-doom-greet.png)

Doom map the `s` (`evil-substitute`) key to `evil-snipe-s`. This is fine if you do not use this key, which is not my case. The `s` key is burned to my soul, and losing it is very uncomfortable. I&rsquo;ll reclaim the key by disabling Evil Snipe (`M-x evil-snipe-mode`) temporarily for now. Later on, I&rsquo;ll make the change permanent.

# Private Config

If you&rsquo;re using a large screen, the default fonts are probably a bit small for your eyes. So let&rsquo;s change that. Since all your settings are stored on `~/.config.el`, lets head to that file by hiting `SPC f p` or `M-x doom/find-file-in-private-config`.
![img](media/02-doom-goto-private-config.png)

`~/.doom.d/config.el` is where you must put all your personal settings, such as variables, functions and keybindings. It comes with some useful instructions that you should probably read.

First, it asks you to set your ID. This is only useful unless your doing things like sending emails from Emacs, but it doesn&rsquo;t hurt to change that just in case.

    (setq user-full-name "Mr. Big"
          user-mail-address "mrbig@email.com")

Besides the Emacs builtins, Doom comes with more than 40 themes for you to chose from. To test them, just hit `SPC h t`. To make the change permanent, alter `(setq doom-theme 'doom-one)` to whatever theme you prefer.

Org Mode uses this variable as a default location for your Org files, for captures and the agenda to use as a reference. Unless you have another preferred location, just leave it as is.

A value of true (`t`) works fine. `relative` can lead to performance issues. I personally don&rsquo;t use line numbers with Emacs. Too troublesome. Instead, I use Avy (either `avy-goto-char-2-above` or `avy-goto-char-2-below` mapped to `F` and `f`, respectively), which is usually much faster anyway. So I leave it like this: `(setq display-line-numbers-type nil)`
The file ends with a brief introduction on Doom&rsquo;s macros. More on that later.

`~/.doom.d/init.el` enables Doom
This file is evaluated before any module. It enables Doom modules and set they&rsquo;re loading order. To view the documentation, press `K` over the module name. If that doesn&rsquo;t work, you can always search for it with `SPC h d m` (`M-x doom/help-modules`).

Some modules have flags (listed on the documentation) that enable related features. They are preceded by `+` and must be added after the module name, with everything inside parentheses. Example (`company` module with the `+childframe` and `+tng` flags):

    (company +childframe
             +tng)

After editing this file, you should quit Emacs and run \`~/.emacs.d/bin/doom sync\` for the changes to take into effect. Alternative, you can run `SPC h r r` (`M-x doom/reload`) to reload your entire session while applying the changes. This command is experimental is doesn&rsquo;t always work.

Doom also provide a nice way to add and manage external packages that are not part of the distribution. `~/.doom.d/packages.el` contain all your package declarations, in the format `(package! package-name)`. It is possible to completely disable packages too, using the format  `(package! package-name :disable t)`. Since I don&rsquo;t like the default keybinding for Evil Snipe (`s`), and since I using Avy anyway, I&rsquo;ll this here:

    (package! evil-snipe :disable t)

After `$ doom sync` or `M-x doom/reload`, `evil-snipe` is completely disabled.

Configurations for those packages should go into `~/.doom.d/config.el`, and I suggest using the `use-package!` macro for. It works just like the regular [use-package](https://jwiegley.github.io/use-package/), with the exception that the block will be ignored if the package is disabled in `~/.doom.d/packages.el`. Here are my settings for Org Mode:

    (use-package! org
      :init
      (add-hook 'org-mode-hook #'pabbrev-mode)
      :custom
      (org-log-into-drawer t)
      (org-ellipsis ".")
      (org-directory "~/org/")
      (org-enforce-todo-checkbox-dependencies t)
      (org-todo-keywords '((sequence "TODO(t)" "STRT(s)" "|" "DONE(d)")))
      :config
      (setq! system-time-locale "C"
             org-capture-templates
             '(("t" "Todo"
                entry
                (file+headline +org-capture-todo-file "Todos")
                "* TODO %? %i" :prepend t)
    
               ("o" "Notes"
                entry
                (file+headline +org-capture-notes-file "Notes")
                "* %u %? %i" :prepend t)
    
               ("j" "Journal"
                entry
                (file+olp+datetree +org-capture-journal-file)
                "* %u %? %i" :prepend t))))

For quick settings, you can use the `after!` macro to run code after a package is loaded.

    (after! python-mode
      (auto-save-mode))

`map!` provides useful shorthand to create keybindings.

`:n` maps to normal mode, `:i` to insert, `v` to visual and so on.

Example: map `C-c 0` to `next-buffer`, but only in normal state.

    (map! :n "C-c 0" 'next-buffer)

The same, but on normal, visual, insert, and emacs states.

    (map! :nvie "C-c 0" 'next-buffer)

Now map `C-c 9` to  `previous-buffer` on normal, visual, insert, emacs, and global states, *but only on Org Mode*.

    (map! :map (org-mode-map)
          :nvieg "C-c 0" 'next-buffer)

`map!` usually work better by themselves, or on the `:init` section of an `use-package!` declaration.

# Basic Commands

## Using the Help System

Doom gives access to most help functions via `SPC h`. Features specific to Doom are available through `SPC h d`. These are especially useful:

-   `SPC h d h` (`M-x doom/help`) : go to user manual.
-   `SPC h d f` (`M-x doom/help-faq`) : search Frequently Asked Questions.
-   `SPC h d S` (`M-x doom/help-search`) : search documentation.
-   `SPC h d s` (`M-x doom/help-search-headings`) : search documentation headline.
-   `SPC h d m` (`M-x doom/help-modules`) : modules documentation.
-   `SPC h d p p` (`M-x doom/help-packages`) : describe packages installed by modules.
-   `SPC h d p h` (`M-x doom/help-package-homepage`) : go to package homepage.

## Other commands

-   `SPC f`: find file prefix.
-   `SPC fr`: find recent files.
-   `SPC fs`: save buffer.
-   `SPC .` (`M-x counsel-find-file`): switch to a buffer visiting a file or creates one if it doesn&rsquo;t exists.
-   `SPC ,` (`M-x ivy-switch-buffer`): switch to another buffer.
